const React = require('react');
const ReactRedux = require('react-redux');

const createActionDispatchers = require('../helpers/createActionDispatchers');
const notebooksActionCreators = require('../reducers/notebooks');
// create the below components
//const Notebook = require('./Notebook');
//const NotebookNew = require('./NotebookNew');

/*
  *** TODO: Build more functionality into the NotebookList component ***
  At the moment, the NotebookList component simply renders the notebooks
  as a plain list containing their titles. This code is just a starting point,
  you will need to build upon it in order to complete the assignment.
*/
class NotebookList extends React.Component {

  constructor(props) {
    super(props);
    // Set initial internal state for this component
    this.state = { loading: false };
  }

  render() {
    const onLoadButtonClick = () => {
      // If we are not already in the process of loading posts,
      // start loading more posts.
      if(!this.state.loading) {
        this.setState({ loading: true });
        this.props.loadMoreNotebooks(() => {
          this.setState({ loading: false });
        });
      }
    };

    // Create a Notebook component from a notebook
    const createNotebookListItem = (notebook) => {
      return (
        <li key={notebook.id}>
          {notebook.title}
        </li>
      )
    }

    return (
      <div>
        <h2>Notebooks</h2>
        <ul>
          {this.props.notebooks.data.map(createNotebookListItem)}
        </ul>
        {/* Button for loading more posts */}
        <button className="blog-load-more btn btn-default btn-lg"
            onClick={onLoadButtonClick}
            disabled={this.state.loading}
          >
            {this.state.loading ? 'Loading...' : 'Load more posts'}
          </button>
      </div>
    );
  }
}

const NotebookListContainer = ReactRedux.connect(
  state => ({
    notebooks: state.notebooks
  }),
  createActionDispatchers(notebooksActionCreators)
)(NotebookList);

module.exports = NotebookListContainer;
