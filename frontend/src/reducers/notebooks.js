const _ = require('lodash');
const api = require('../helpers/api');

// Action type constants
/* *** TODO: Put action constants here *** */
const INSERT = 'frontend/notebooks/INSERT';
const CHANGE = 'frontend/notebooks/CHANGE';
const REMOVE = 'frontend/notebooks/REMOVE';

const initialState = {
  notebookData: [
    //{ id: 100, title: 'From Redux Store: A hard-coded notebook' },
    //{ id: 101, title: 'From Redux Store: Another hard-coded notebook' },
    {
      notebookId: -1,
      activeNote: null,
      notes: [],
    }

  ]
};

// Function which takes the current data state and an action,
// and returns a new state
function reducer(state, action) {
  state = state || initialState;
  action = action || {};

  switch(action.type) {
    /* *** TODO: Put per-action code here *** */
    // Insert new Notebook into the local store
    case INSERT: {
      const unsortedNotebooks = _.concat(state.notebookData, action.notebooks);
      const notebookData = _.orderBy(unsortedNotebooks, 'createdAt', 'desc');

      return _.assign({}, state, { notebookData });
    }

    // Change a single Notebook's data in the local store
    case CHANGE: {
      const notebookData = _.clone(state.notebookData);
      const changedIndex = _.findIndex(state.notebookData, {id: action.notebook.id})
      notebookData[changedIndex] = action.notebook;
      return _.assign({}, state, { notebookData });
    }

    // Remove a single Notebook from the noteBookData list
    case REMOVE: {
      const notebookData = _.reject(state.notebookData, {id: action.id});
      return _.assign({}, state, { notebookData });
    }

    default: return state;
  }
}

// Action creators
/* *** TODO: Put action creators here *** */

// Insert notebook into the list of Notebooks
reducer.insertNotebook = (notebooks) => {
  return { type: INSERT, notebooks};
};

// Remove notebook from the list of Notebooks
reducer.removeNotebook = (id) => {
  return { type: REMOVE, id };
}

// Delete a Notebook from the api
reducer.deleteNotebook = (notebookId) => {
  return (dispatch) => {
    api.delete('/notebooks/' + notebookId).then(() => {
      dispatch(reducer.removeNotebook(notebookId));
    })
  }
}

// Changes local Notebook data
reducer.changeNotebook = (notebook) => {
  return { type: CHANGE, notebook };
};

// Update a Notebook in the server
reducer.saveNotebook = (editedNotebook, callback) => {
  return (dispatch) => {
    api.put('/notebooks/' + editedNotebook.id, editedNoteook).then((notebook) => {
      // Saves local post.
      dispatch(reducer.changeNotebook(notebook));
      callback();
    }).catch(() => {
      alert('Failed to save post. Are all of the fields filled in correctly?');
    });
  };
};

// Create a new Notebook and then insert it into the list
reducer.createNotebook = (newNotebook, callback) => {
  return (dispatch) => {
    api.post('/notebooks/', newNotebook).then((notebook) => {
      // This post is one that the store returns us! It has post id incremented to the next available id
      dispatch(reducer.insertNotebook([notebook]));
      callback();
    }).catch(() => {
      alert('Failed to create post. Are all of the fields filled in correctly?');
    });
  };
};

// Load more Notebooks from the server
reducer.loadMoreNotebooks = (callback) => {
  return (dispatch, getState) => {
    const state = _.assign({}, initialState, getState().notebooks);

    let path = '/notebooks';
    if (state.notebookData.length > 0) {
        const oldestNotebook =_.last(state.notebookData);
        path = '/notebooks?olderThan=' + oldestNotebook.createdAt;
    }
    api.get(path).then((newNotebooks) => {
      dispatch(reducer.insertNotebook(newNotebooks));
      callback();
    }).catch(() => {
      alert('Failed to load more posts');
      callback('Failed to load more posts');
    });

  };


};

// Export the action creators and reducer
module.exports = reducer;
